#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Ruská ruleta se zápočty studentů."""

import time
import random


def zapocet(uco):
    """Roztočení bubnu, natažení kohoutku a stisk spouště"""
    seed = time.time() + uco
    random.seed(seed)
    return random.choice((True, False))


if __name__ == "__main__":

    import os
    import sys
    import time

    with open(sys.argv[1]) as seznam:
        studenti = seznam.readlines()

    celkem = len(studenti)
    p = 0

    while p/celkem < 0.7:
        os.system("clear")
        p = 0
        print("F0020 Podzimní astronomický kurs\n")
        for i, student in enumerate(studenti):
            prosel = zapocet(int(student))
            if prosel:
                print("{num:2d}. Student číslo {uco} ..... prošel"
                      .format(num=i, uco=student.rstrip("\n")))
                p += 1
            else:
                print("{:2d}. Student číslo {} ... neprošel"
                      .format(i, student.rstrip()))

        print("\nZápočet získalo {2} z {1} tj. {0:2.0f}% studentů."
              .format(p/celkem*100, celkem, p))

        time.sleep(0.5)


# -------------------------------------------------------------------------- #
# "THE BEER-WARE LICENSE" (Revision 42):                                     #
# <janak@physics.muni.cz> wrote this file. As long as you retain this notice #
# you can do whatever you want with this stuff. If we meet some day, and you #
# think this stuff is worth it, you can buy me a beer in return Zdeněk Janák #
# -------------------------------------------------------------------------- #
