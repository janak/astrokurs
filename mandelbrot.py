#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Mandelbrotova množina
=====================

**Mandelbrotova množina** je množina bodů komplexní roviny, které jsou odvozeny od rekurzivních procesů s komplexními čísly patřícími této množině a jejímu okolí. Mandelbrotova množina je jeden z nejznámějších fraktálů, přesněji řečeno fraktálem je její okraj. K jejímu určení se používá zobrazení, které každému komplexnímu číslu $c$ přiřazuje určitou posloupnost komplexních čísel $z_n$. Tato posloupnost je určena následujícím rekurzivním předpisem:

$$z_0 = 0, \quad z_{n+1} = z_n^2 + c$$

Madelbrodova množina je pak definována jako množina komplexních čísel $c$, pro která je posloupnost $z_0, z_1, z_2, \dots$  omezená, tj. že splňuje následující podmínku:

Existuje reálné číslo $m$ takové, že pro všechna $n$ je $|z_n| \le m$.

Lze dokázat, že překročí-li absolutní hodnota některého členu posloupnosti $z_n$ hodnotu 2, pak tato poslupnost není omezená (jde do nekonečna). Odtud je zřejmé, že lze ve výše uvedené definici položit $m = 2$, aniž by tím došlo ke změně jejího významu.

## Reference

* Přispěvatelé Wikipedie, *Mandelbrotova množina* [online], Wikipedie: Otevřená encyklopedie, c2013, Datum poslední revize 8. 03. 2013, 22:19 UTC, [citováno 13. 03. 2013] <http://cs.wikipedia.org/w/index.php?title=Mandelbrotova_množina>

* Ondřej Čertík, *Mandelbrot set* [online], Fortran 90 -- Fortran90 1.0 documentation. [citováno 13. 03. 2013] <http://fortran90.org/src/rosetta.html#mandelbrot-set>

"""

import numpy as np

ITERATIONS = 100
DENSITY = 1000
x_min, x_max = -2.68, 1.32
y_min, y_max = -1.5, 1.5

x, y = np.meshgrid(np.linspace(x_min, x_max, DENSITY),
                   np.linspace(y_min, y_max, DENSITY))
c = x + 1j*y
z = c.copy()
fractal = np.zeros(z.shape, dtype=np.uint8) + 255

for n in range(ITERATIONS):
    print("Iteration {}".format(n))
    mask = abs(z) <= 10
    z[mask] *= z[mask]
    z[mask] += c[mask]
    fractal[(fractal == 255) & (-mask)] = 254. * n / ITERATIONS

print("Saving...")
np.savetxt("fractal.dat", np.log(fractal))
np.savetxt("coord.dat", [x_min, x_max, y_min, y_max])

# --------------------------------------------------------------------------- #

from numpy import loadtxt
import matplotlib.pyplot as plt

fractal = loadtxt("fractal.dat")
x_min, x_max, y_min, y_max = loadtxt("coord.dat")

plt.imshow(fractal, cmap=plt.cm.hot,
           extent=(x_min, x_max, y_min, y_max))
plt.title('Mandelbrot Set')
plt.xlabel('Re(z)')
plt.ylabel('Im(z)')
plt.savefig("mandelbrot.png")
plt.show()
