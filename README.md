Všechno, co jste kdy chtěli vědět o Pythonu, ale báli jste se zeptat
====================================================================

## Co je Python?

> Python je dynamický objektově orientovaný skriptovací programovací jazyk, který v roce 1991 navrhl Guido van Rossum. Python je vyvíjen jako open source projekt, který zdarma nabízí instalační balíky pro většinu běžných platforem (Unix, Windows, Mac OS); ve většině distribucí systému Linux je Python součástí základní instalace.
> -- *[Wikipedie](http://cs.wikipedia.org/wiki/Python)*

## Kde ho získat?

* [Download Python](http://www.python.org/download/) -- The current production versions are Python 2.7.5 and Python 3.3.2.
* [Python(x,y)](https://code.google.com/p/pythonxy/) -- Python(x,y) is a free scientific and engineering development software for numerical computations, data analysis and data visualization based on Python programming language, Qt graphical user interfaces and Spyder interactive scientific development environment.
* [Enthought Canopy](https://www.enthought.com/products/canopy/) -- Enthought Canopy is a comprehensive Python analysis environment with easy installation & updates of the proven Enthought Python distribution - all part of a robust platform you can explore, develop and visualize on.
* `$ sudo apt-get install python` -- Debian, Ubuntu, Mint, ...

## Co ještě budu potřebovat?

* [IPython](http://ipython.org/) -- Enhanced Interactive Console
* [NumPy](http://www.numpy.org/) -- Base N-dimensional array package
* [SciPy](http://www.scipy.org/) -- Fundamental library for scientific computing
* [Matplotlib](http://matplotlib.org/xkcd/) -- Comprehensive 2D Plotting
* [PyFITS](http://www.stsci.edu/institute/software_hardware/pyfits) -- The PyFITS module is a Python library providing access to FITS files. FITS (Flexible Image Transport System) is a portable file standard widely used in the astronomy community to store images and tables.

## Kdo ho používá?

* [CASA](http://casa.nrao.edu/) -- *Common Astronomy Software Applications* is a comprehensive software package to calibrate, image, and analyze radioastronomical data from interferometers (such as ALMA and EVLA) as well as single dish telescopes.
* [PyRAF](http://www.stsci.edu/institute/software_hardware/pyraf/) -- PyRAF is a command language for running IRAF tasks that is based on the Python scripting language.
* [Astropy](http://www.astropy.org/) -- The Astropy Project is a community effort to develop a single core package for Astronomy in Python and foster interoperability between Python astronomy packages.

## Co ještě umí?

* [SymPy](http://sympy.org/) -- SymPy is a Python library for symbolic mathematics.
* [scikit-learn](http://scikit-learn.org/) -- Machine Learning in Python
* [AstroML](http://www.astroml.org/) -- Machine Learning and Data Mining for Astronomy

## Proč se jej učit?

> Čím více (programovacích) jazyků umíš, tím více jsi robotem.
> -- *Mgr. Viktor Votruba, Ph.D.*

## Jaký je?

    >>> import antigravity

![Python](http://imgs.xkcd.com/comics/python.png "I wrote 20 short programs in Python yesterday. It was wonderful. Perl, I'm leaving you.")

    >>> import this

> The Zen of Python, by Tim Peters
> 
> - Beautiful is better than ugly.
> - Explicit is better than implicit.
> - Simple is better than complex.
> - Complex is better than complicated.
> - Flat is better than nested.
> - Sparse is better than dense.
> - Readability counts.
> - Special cases aren't special enough to break the rules.
> - Although practicality beats purity.
> - Errors should never pass silently.
> - Unless explicitly silenced.
> - In the face of ambiguity, refuse the temptation to guess.
> - There should be one-- and preferably only one --obvious way to do it.
> - Although that way may not be obvious at first unless you're Dutch.
> - Now is better than never.
> - Although never is often better than *right* now.
> - If the implementation is hard to explain, it's a bad idea.
> - If the implementation is easy to explain, it may be a good idea.
> - Namespaces are one honking great idea -- let's do more of those!

## Jak vypadá?

* `$ python`
* `$ python mandelbrot.py`
* `$ ipython`
* `$ ipython qtconsole --pylab inline`
* `$ ipython notebook --pylab=inline Mandelbrot.ipynb`

